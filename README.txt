A module to make KINGdesk's typography [1] available for Drupal.

PHP Typography corrects web typography with the following respects:
 * Hyphenation
 * Spacing control, including: gluing values to units, widow protection, and
   forced internal wrapping of long URLs & email addresses.
 * Intelligent character replacement, including smart handling of: quote marks
   (“foo”), dashes (foo – bar), ellipses (…), trademarks (™), math symbols
   (1024×768), fractions (12⁄23), and ordinal suffixes (3rd)
 * CSS hooks for styling: ampersands (class "amp"), acronyms (class "caps"),
   numbers (class "numbers"), initial single quotes (class "quo"), and initial
   double quotes & guillemets (class "dquo").

This module …
 * makes PHP Typography available in Drupal
 * provides fine-grained settings (all from [2])
 * allows to set the quotation marks on a per language basis

[1] http://kingdesk.com/projects/php-typography/
[2] http://kingdesk.com/projects/php-typography-documentation/class-phptypography/settings-methods
